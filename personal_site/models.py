from django.db import models
import re
# Create your models here.





class Basic(models.Model):
	title = models.CharField(max_length=100)
	pub_date = models.DateTimeField('date published')
	order = models.IntegerField()

	def __unicode__(self):  # Python 3: def __str__(self):
		return self.title

	def id2(self):
		return re.sub(r'\s+', '', self.title)

	class Meta:
		abstract = True

class Title(Basic):
	sub_title = models.CharField(max_length=100)

class BasicTextSection(Basic):

	main_body = models.TextField(max_length=5000)


	def __unicode__(self):  # Python 3: def __str__(self):
		return self.title





class Choice(models.Model):
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    def __unicode__(self):  # Python 3: def __str__(self):
       return self.choice_text