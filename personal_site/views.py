from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from personal_site.models import BasicTextSection, Title
from django.template import RequestContext, loader

def index(request):
	title = Title.objects.order_by('order')[:1]
	sections = BasicTextSection.objects.order_by('order')
	template = loader.get_template('personal_site/index.html')
	context = {'sections': sections, 'title': title}
	return render(request, 'personal_site/index.html', context)
	"""
    latest_poll_list = Poll.objects.order_by('-pub_date')[:5]
    template = loader.get_template('polls/index.html')
    context = {'latest_poll_list': latest_poll_list}
    """

