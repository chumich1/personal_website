from django.contrib import admin
from personal_site.models import BasicTextSection, Title
from projects.models import Project, ProjectSection

class ProjSectionInline(admin.StackedInline):
    model = ProjectSection
    extra = 3



class ProjectAdmin(admin.ModelAdmin):
	fieldsets = [
	(None,               {'fields': ['title', 'url']}),
	('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
	]
	list_display = ('title', 'pub_date', 'url')
	search_list = ['title', 'pub_date']
	inlines = [ProjSectionInline]
	list_filter = ['pub_date']
	search_fields = search_list



admin.site.register(Project, ProjectAdmin)
