from django.db import models
import re

# Create your models here.
class Project(models.Model):
	title = models.CharField(max_length=100)
	pub_date = models.DateTimeField('date published')
	url = models.URLField()
	def __unicode__(self):  # Python 3: def __str__(self):
		return(self.title)


class ProjectSection(models.Model):
	proj = models.ForeignKey(Project)
	section_title = models.CharField(max_length=100)
	main_body = models.TextField(max_length=5000)
	order = models.IntegerField()


	def id2(self):
		return re.sub(r'\s+', '', self.section_title)

	def __unicode__(self):  # Python 3: def __str__(self):
		return(self.main_body)