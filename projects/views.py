from django.shortcuts import render, get_object_or_404, get_list_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from projects.models import ProjectSection, Project
from django.template import RequestContext, loader
# Create your views here.



def index(request):
    latest_project_list = Project.objects.order_by('-pub_date')[:5]
    template = loader.get_template('projects/index.html')
    context = {'latest_project_list': latest_project_list}
    return render(request, 'projects/index.html', context)

def detail(request, project_id):
	project = get_object_or_404(Project, pk=project_id)
	context = {'project': project}
	return render(request, 'projects/detail.html', context)