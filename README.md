************** READ ME File ******************

Tuesday, April 15

Personal Website
0.1

System Requirements:
Django 1.6
Python 2.7

How To Run:
$ python manage.py runserver

New Features:
Home Page
Side Menu
Dynamic Project Section

Note:
This is running on a test server and is not yet ready for a production environment