from django.contrib import admin
from polls.models import Poll
from polls.models import Choice

# Register your models here.

class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3

class PollAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['question']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    list_display = ('question', 'pub_date', 'was_published_recently')
    search_list = ['question', 'pub_date']
    inlines = [ChoiceInline]
    list_filter = ['pub_date']
    search_fields = search_list


#admin.site.register(Choice)
admin.site.register(Poll, PollAdmin)
